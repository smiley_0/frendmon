#!/usr/bin/env python3
# SenseHat PiNode

import argparse

import requests
import platform

import time

from sense_hat import SenseHat
hat = SenseHat()

ENDP_API = "/api"
ENDP_PING = ENDP_API + "/ping/"
ENDP_FIRST_CONTACT = ENDP_API + "/first-contact/"
ENDP_MEASUREMENT = ENDP_API + "/measurement/"


class Endpoint(object):
    def __init__(self, chipid):
        self.endpoint = None
        self.chipid = chipid
        self.interval = -1

    def set_address(self, endpoint):
        r = requests.get(endpoint + ENDP_PING)
        if r.status_code == 200:
            self.endpoint = endpoint
            return True

        return False

    def send_and_parse(self, addr, payload):
        r = requests.post(self.endpoint + addr, json=payload)
        if r.status_code != 200:
            return False

        data = r.json()
        if data.get('status') != 0:
            print('[Endpoint] Received error from server')
            print(data.get('what'))
            return False

        if data.get('int') is not None:
            self.interval = data.get('int')

        return True

    def send_first_contact(self):
        payload = {
            'id': self.chipid,
            'cpu': 0,
            'sdk': '{} {}'.format(platform.python_implementation(), platform.python_version()),
            'core': '{} {}'.format(platform.system(), platform.release()),
            'rst': 'null',
            'fw': 32*'f',
            'fws': 0
        }
        return self.send_and_parse(ENDP_FIRST_CONTACT, payload)

    def send_measurement_update(self, temp_c, humidity_p, pressure_mbar):
        payload = {
            'id': self.chipid,
            'temp': temp_c,
            'hum': humidity_p,
            'pres': pressure_mbar,
        }
        return self.send_and_parse(ENDP_MEASUREMENT, payload)


def get_args():
    parser = argparse.ArgumentParser(description='Raspberry Pi SenseHat Node')
    parser.add_argument('-c', '--chipid', type=int, help='ESP8266 Chip ID', required=True)
    parser.add_argument('-e', '--endpoint', type=str, help='API Endpoint', required=True)
    return parser.parse_args()


def loop(endpoint):
    while True:
        temp = hat.get_temperature()
        humidity = hat.get_humidity()
        pressure = hat.get_pressure()

        print('[Update]', f'T: {temp} H: {humidity} P: {pressure} ', end='')

        if endpoint.send_measurement_update(temp, humidity, pressure):
            print('[ OK ]')
        else:
            print('[FAIL]')

        time.sleep(endpoint.interval)


def main():
    args = get_args()

    print('Starting...')
    endpoint = Endpoint(args.chipid)

    if not endpoint.set_address(args.endpoint):
        print('[Endpoint] Unable to connect')
        exit(1)
    else:
        print('[Endpoint] Server check OK')

    print('[Setup] Making first contact...')
    succ = False
    for x in range(5):
        if x > 0:
            print('[Setup] Failed. Waiting 5s before retry')
            time.sleep(5)

        print('[Setup] Attempt {}...'.format(x+1))
        if endpoint.send_first_contact():
            succ = True
            break

    if succ and endpoint.interval != -1:
        print('[Setup] Made first contact. Initial interval is {}ms'.format(endpoint.interval))
    else:
        print('[Setup] First contact failed.')
        exit(1)

    print('[Setup] All done, starting main loop')
    loop(endpoint)


if __name__ == '__main__':
    main()
