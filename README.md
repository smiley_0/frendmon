# Cloud-based temperature monitoring

This project implements a client/server based architecture for monitoring temperature using ESP8266-based nodes.

## Description

The server side, implemented in Python with use of the Django web framework, enables centralized management of nodes and collection of data from sensors. All communication is initiated by the nodes, which enables collection of data from nodes without a public Internet address. The **Hypertext Transfer Protocol (HTTP)** is used as the application layer protocol, while the messages itself are serialized using the **JavaScript Object Notation (JSON)**.

The client side, implemented as a firmware for an *WEMOS D1 mini* based node, periodically measures the temperature using the *DS18B20+* sensor and sends the measurement to the server.

Additionaly, a Python-based node simulator, which can be used for simulation of multiple nodes or when a hardware node is not available, has been created and included with the project.

The lifetime of the node, excluding the configuration mode (described later in the text), is following:

1. The node verifies connectivity to server by sending an empty GET request to the server on a specified URL. If the server responds with success, the node continues
2. The node sends a *first contact* message. Purpose of this message is to verify that the node is already registered and known by the server, using the nodes *Chip ID*. This message also contains various hardware and firmware information, such as the CPU frequency, SDK version or firmware MD5.
3. If the node is known by the server, server responds with success. The response also contains an initial *interval* value, used by node in the next step.
4. The node periodically (with the interval specified by server) measures the temperature using its sensor and sends the measurement in a *measurement update* message. To identify itself, the node adds its *Chip ID* as a part of the message.
5. The server can include a new, updated *interval* value as a part of a response to successful *measurement update* message, which is then immediately used by node.

## Installation

### Server
The server part requires Python 3.6+. A recommended way to install dependencies is using [virtualenv](https://virtualenv.pypa.io/):

```
$ virtualenv env
(env) $ source env/bin/activate
(env) $ pip install -r server/requirements.txt
```

Due to the Django's built-in protection against HTTP Host header attacks, it is necessary to edit the [`ALLOWED_HOSTS`](https://docs.djangoproject.com/en/1.11/ref/settings/#allowed-hosts) option in server settings (`server/server/settings.py`) to include all IP addresses and hostnames on which the server should be accessible. For example:

```
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]', '192.168.42.47', 'tempmon.example.org']

```

Next, create an initial database by applying migrations and a superuser account by running:

```
(env) $ cd server
(env) $ python manage.py migrate
(env) $ python manage.py createsuperuser
```

Finally, the built-in Django development server can be started:

```
(env) $ python manage.py runserver 0.0.0.0:8000
```

In this case, the server will listen on all IP addresses and port 8000. Additional options can be found be running `python manage.py runserver --help`.

While the built-in development server is sufficient for testing, its use is not recommended for production use. For long-term production use, refer to [Django documentation on deployment](https://docs.djangoproject.com/en/1.11/howto/deployment/).

### Python-based node simulator
The node simulator has a separate set of dependencies independent from the server side, however, they can be installed to the same virtualenv environment:

```
(env) $ pip install -r fakenode/requirements.txt
```

The node can be then launched by running `node.py`:

```
(env) $ python fakenode/node.py -c 42047 -e "http://tempmon.example.org:8000"
```

The `-c` option specifies an ESP8266 Chip ID that will be used for identification with server specified by the `-e` option. Both options are required.

### WEMOS D1 mini-based node

The node firmware has the following dependencies that should be installed using the [Arduino Library Manager](https://www.arduino.cc/en/Guide/Libraries#toc3):

- [ArduinoJSON][aj] 5.11.2
- [DallasTemperature][dt] 3.7.6
- [OneWire][1w] 2.3.3
- [WiFiManager][wm] 0.12.0

[aj]: https://github.com/bblanchon/ArduinoJson
[dt]: https://github.com/milesburton/Arduino-Temperature-Control-Library
[1w]: https://github.com/PaulStoffregen/OneWire
[wm]: https://github.com/tzapu/WiFiManager


## Usage
First, the successful installation of server should be verified by accessing the servers web interface on IP/hostname and port used during installation and logging in into the *Admin* section using the created superuser account.

### Adding a node
A properly programmed node should immediately start broadcasting a secured WiFi network with SSID `ESPConfig` used for initial configuration of node. Please note that it may be more convenient (or even necessary) to use a different device than the one running the server during the configuration.

After connecting using password `tazke heslo`, a captive portal should be displayed either automatically, or by visiting any web page in your web browser. Choose the *Info* option and take note of the *Chip ID* parameter.

Log in to the *Admin* section of the server web interface and click *Add sensor* button (either in the *Admin* section or homepage). Fill in the Chip ID obtained in previous step and name the device. You may also fill in the rest of details, though this is not required and may be done at any time after the configuration is done.

Back in the captive portal, choose *Configure WiFi*. Select the desired WiFi network, fill in the password and specify URL of the server that will collect data from this node (i.e. `http://tempmon.example.org:8000`), and click *Save*

If the node was able to successfully connect to the WiFi network and establish connection with the server, it will stop broadcasting the `ESPConfig` network and start sending data to server. Otherwise, the node will continue in the configuration mode until successfully configured.

The configuration is stored in the permanent memory of node and will be used during the next start of node.

### Working with node
A successfully configured and running node will periodically send updates to the server. These data are publicly available from the servers web interface in form of measurements, aggregated statistics and graphs.

As logged in user, you can also modify configuration of the node and view hardware information sent in the *first contact* message in the *Admin* section.

The *Interval* option is particularly important, as it determines the amount of time in seconds the node waits between updates. After changing the *Interval* option, it will sent as a part of a response to the next *measurement update* message.

### Re-configuring the node
Due to the fact that there is only one hardware button on the WEMOS D1 mini, already used as a soft reset, the only way to trigger the captive portal after successful configuration is by restarting the node with either the WiFi network or server inaccessible.

### Removing the node
The node may be removed from the server at any time from the *Admin* section of the web interface. This will however only stop server from receiving and saving the data from node and leave the local configuration of node intact.


## Security considerations
Due to the nature of the project, no extra measures were taken to secure the communication of client with server and the communication is not encrypted.

The fact that no encryption is employed during the transfer is not very concerning on its own, as the temperature measurements are publicly viewable from the web interface.

However, the only mechanism employed for identification of node by server is its Chip ID, which is also transferred unencrypted, in its full form. This makes it trivial to obtain the ID by analyzing the nodes communication and use it for impersonation of the node.

Another issue is the interval value sent in the response of *first contact* or *measurement update* request, as an attacker could modify its value to a sufficiently large number, effectively rendering the node inactive for a long time.

These issues could be, in fact, resolved by the use of TLS/SSL, this would however require additional steps during the configuration of server side. Another factor against encryption is the limited processing power of the CPU.

An alternative, more lightweight solution would be signing all required messages using some sort of unique, private key. In this case, it is however necessary to consider the possibility of replay attacks and ways to prevent them.

One of the ways message signing could be implemented is, for example, the use of [JSON Web Tokens](https://jwt.io).


## Author
Adrian Kiraly, <xkiral01@stud.fit.vutbr.cz>
