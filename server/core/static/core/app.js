// Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
// [original]
// Last change: 2017-11-03

$(document).ready(function ($) {
    "use strict";
    // Hamburger menu
    $(".navbar-burger").click(function () {
        const target = $("#" + $(this).data("target"));
        const burger = $(this);
        burger.toggleClass("is-active");
        target.toggleClass("is-active");
        target.click(function () {
            burger.removeClass("is-active");
            $(this).removeClass("is-active");
        });
    });

    // Setup temporary dygraphStart function
    if (!window.dygraphStart) {
        window.dygraphStart = function () {return false;};
    } else {
        dygraphStart();
    }

    // Setup pjax
    $(document).pjax("a", "#pjaxContainer", {timeout: 6000});

    $(document).on("pjax:end", function() {
        dygraphStart();
        window.dygraphStart = function () {return false;};
    });

    // Autorefresh
    var arInterval = -1;
    $(".autoRefreshControl").click(function () {
        if (arInterval === -1) {
            // Turn on
            $(this).addClass("has-text-info");
            arInterval = setInterval(function () {
                $.pjax.reload("#pjaxContainer");
            }, 10000);
        } else {
            // Turn off
            $(this).removeClass("has-text-info");
            clearInterval(arInterval);
            arInterval = -1;
        }
    })
});
