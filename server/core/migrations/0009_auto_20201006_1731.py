# Generated by Django 3.1.2 on 2020-10-06 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_sensor_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='measurement',
            name='humidity',
            field=models.DecimalField(decimal_places=4, default=0, editable=False, max_digits=8),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='measurement',
            name='pressure',
            field=models.DecimalField(decimal_places=4, default=0, editable=False, max_digits=9),
            preserve_default=False,
        ),
    ]
