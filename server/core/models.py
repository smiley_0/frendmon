# Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
# [original]
# Last change: 2017-11-03

from django.db import models

# Create your models here.
from django.utils import timezone


class Sensor(models.Model):
    COLOR_CHOICES = (
        ('is-primary', 'Turquoise'),
        ('is-link', 'Blue'),
        ('is-info', 'Cyan'),
        ('is-success', 'Green'),
        ('is-warning', 'Yellow'),
        ('is-danger', 'Red'),
        ('is-light', 'White'),
        ('is-dark', 'Black'),
    )

    # Identification
    chip_id = models.PositiveIntegerField(unique=True)

    # User supplied info
    name = models.CharField(max_length=256)
    color = models.CharField(max_length=32, choices=COLOR_CHOICES, default=COLOR_CHOICES[0][0])
    location = models.CharField(max_length=256, blank=True, null=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)

    # OTA Config parameters
    interval = models.PositiveIntegerField(default=60)
    refresh_required = models.BooleanField(default=False, editable=False)

    # HW Info
    boot_time = models.DateTimeField(blank=True, null=True, editable=False)
    cpu_mhz = models.PositiveIntegerField(blank=True, null=True, editable=False)
    sdk_version = models.CharField(max_length=32, blank=True, null=True, editable=False)
    core_version = models.CharField(max_length=32, blank=True, null=True, editable=False)
    reset_info = models.CharField(max_length=512, blank=True, null=True, editable=False)
    firmware_md5 = models.CharField(max_length=32, blank=True, null=True, editable=False)
    firmware_size = models.PositiveIntegerField(blank=True, null=True, editable=False)
    # TODO more stuff

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('core:sensor', args=[str(self.id)])

    def is_online(self):
        if self.boot_time is None:
            return False
        else:
            delta = timezone.now() - self.measurement_set.first().timestamp
            return delta.total_seconds() < (self.interval + 5)


class Measurement(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE, editable=False)
    timestamp = models.DateTimeField(auto_now=True, editable=False)
    temperature = models.DecimalField(max_digits=7, decimal_places=4, editable=False)
    humidity = models.DecimalField(max_digits=8, decimal_places=4, editable=False)
    pressure = models.DecimalField(max_digits=9, decimal_places=4, editable=False)

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return "Measurement #{}".format(self.id)
