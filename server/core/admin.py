# Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
# [original]
# Last change: 2017-11-03

from django.contrib import admin

# Register your models here.
from .models import Sensor, Measurement


class MeasurementInline(admin.TabularInline):
    model = Measurement
    extra = 0
    max_num = 15
    classes = ('collapse',)
    readonly_fields = ('timestamp', 'temperature')


class SensorAdmin(admin.ModelAdmin):
    HWINFO_FIELDS = (
        'boot_time',
        'cpu_mhz',
        'sdk_version',
        'core_version',
        'reset_info',
        'firmware_md5',
        'firmware_size'
    )

    fieldsets = (
        (None, {
            'fields': ('chip_id', 'name', 'color'),
        }),
        ('Location', {
            'fields': ('location', 'latitude', 'longitude'),
        }),
        ('Configuration', {
            'fields': ('interval',),
        }),
        ('Hardware info', {
            'classes': ('collapse',),
            'fields': HWINFO_FIELDS,
        }),
    )
    readonly_fields = HWINFO_FIELDS
    list_display = ('name', 'interval', 'refresh_required', 'boot_time', 'sdk_version', 'core_version', 'firmware_md5')
    list_filter = ('interval', 'boot_time', 'sdk_version', 'core_version', 'firmware_md5')
    search_fields = ('chip_id', 'name')

    # Disabled - too slow
    # inlines = (MeasurementInline,)

    def save_model(self, request, obj, form, change):
        if change and 'interval' in form.changed_data:
            obj.refresh_required = True

        super().save_model(request, obj, form, change)


admin.site.register(Sensor, SensorAdmin)
