# Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
# [original]
# Last change: 2017-11-03

from .models import Sensor


def sensor_list(request):
    sensors = Sensor.objects.all()
    return {
        'sensors': sensors,
    }
