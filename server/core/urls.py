# Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
# [original]
# Last change: 2017-11-03

from django.conf.urls import url

from . import views

app_name = 'core'
urlpatterns = [
    url(r'^$', views.sensor_list, name='sensors'),
    url(r'^sensor/(?P<pk>\d+)/$', views.SensorView.as_view(), name='sensor'),
    url(r'^sensor/(?P<pk>\d+)/page/(?P<page>\d+)/$', views.SensorView.as_view(), name='sensor_page'),
    url(r'^sensor/(?P<pk>\d+)/graph/$', views.GraphView.as_view(), name='graph'),
    # API endpoints
    url(r'^api/ping/$', views.api_ping, name='api_ping'),
    url(r'^api/first-contact/$', views.api_firstcontact, name='api_firstcontact'),
    url(r'^api/measurement/$', views.api_measurement, name='api_measurement'),
    url(r'^api/sensor/(?P<pk>\d+)/csv/$', views.api_sensor_csv, name='api_sensor_csv'),
]
