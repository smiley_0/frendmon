# Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
# [original]
# Last change: 2017-11-03

import csv
import json

from django.db.models import Max, Min, Count, Avg
from django.http import HttpResponse, JsonResponse, StreamingHttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone, formats
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .models import Sensor, Measurement


def sensor_list(request):
    return render(request, 'core/sensors.html', None)


class SensorView(generic.ListView):
    template_name = 'core/sensor.html'
    context_object_name = 'measurements'
    paginate_by = 30

    def get_queryset(self):
        self.sensor = get_object_or_404(Sensor, id=self.kwargs['pk'])
        return Measurement.objects.filter(sensor=self.sensor)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sensor'] = self.sensor

        # Include stats
        context['stats'] = self.sensor.measurement_set.aggregate(
            count=Count('id'),
            min=Min('temperature'),
            max=Max('temperature'),
        )

        delta = timezone.now() - timezone.timedelta(days=1)
        context['stats'].update(self.sensor.measurement_set.filter(timestamp__gte=delta).aggregate(
            avg=Avg('temperature'),
        ))

        return context


class GraphView(generic.DetailView):
    model = Sensor
    template_name = 'core/graph.html'


# API Endpoints

def api_ping(request):
    """Returns simple 200 OK message, only used for connectivity check"""
    return HttpResponse()


@require_POST
@csrf_exempt
def api_firstcontact(request):
    response = {
        'status': 0,
    }

    # Find sensor in db
    try:
        data = json.loads(request.body.decode('utf-8'))
        sensor = Sensor.objects.get(chip_id=data['id'])
    except Exception as e:
        response['status'] = 1
        response['what'] = str(e)
        return JsonResponse(response)

    # Save current hardware info
    sensor.boot_time = timezone.now()
    sensor.cpu_mhz = data.get('cpu')
    sensor.sdk_version = data.get('sdk')
    sensor.core_version = data.get('core')
    sensor.reset_info = data.get('rst')
    sensor.firmware_md5 = data.get('fw')
    sensor.firmware_size = data.get('fws')
    sensor.save()

    # Add interval to response
    response['int'] = sensor.interval

    return JsonResponse(response)


@require_POST
@csrf_exempt
def api_measurement(request):
    response = {
        'status': 0,
    }

    try:
        data = json.loads(request.body.decode('utf-8'))
        sensor = Sensor.objects.get(chip_id=data['id'])
        measurement = Measurement(
            sensor=sensor,
            temperature=data['temp'],
            humidity=data['hum'],
            pressure=data['pres'],
        )
        measurement.save()
    except Exception as e:
        response['status'] = 1
        response['what'] = str(e)
        return JsonResponse(response)

    if sensor.refresh_required:
        response['int'] = sensor.interval
        sensor.refresh_required = False
        sensor.save()

    return JsonResponse(response)


class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def api_sensor_csv(request, pk):
    sensor = get_object_or_404(Sensor, id=pk)
    data =  sensor.measurement_set.all().order_by('timestamp')
    rows = ([formats.date_format(m.timestamp, "Y-m-d H:i:s"), m.temperature, m.humidity, m.pressure] for m in data)

    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    response = StreamingHttpResponse(
        (writer.writerow(row) for row in rows),
        content_type="text/csv"
    )
    response['Content-Disposition'] = 'attachment; filename="sensordata.csv"'
    return response
