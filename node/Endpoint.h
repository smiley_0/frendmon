// Endpoint class header file
// Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
// [original]
// Last change: 2017-11-02

#ifndef Endpoint_h
#define Endpoint_h

#include <Arduino.h>

class Endpoint {
public:
    int getInterval();
    String getAddress();
    bool setAddress(const String &address);

    bool sendFirstContact();
    bool sendMeasurementUpdate(float tempC);

private:
    String address;
    int interval = -1;

    bool parseResponse(const String &response);
    bool postMessageAndParse(const String &apiPath, const String &request);
};

#endif // Endpoint_h
