// Node firmware main file
// Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
// [original]
// Last change: 2017-11-02

#include <Arduino.h>

// Config storage
#include <FS.h>

// WiFiManager
#include <ESP8266WiFi.h>
#include <WiFiManager.h>

// DallasTemperature
#include <OneWire.h>
#include <DallasTemperature.h>

// Custom stuff
#include "device.h"
#include "Endpoint.h"

// Sensors
OneWire oneWire(pinThermo);
DallasTemperature sensors(&oneWire);
DeviceAddress thermo;

// Endpoint manager
Endpoint endpoint;
bool shouldSave = false;    // Has to be global, lambda capture doesn't work when passing as function pointer

void setup() {
    Serial.begin(115200);
    Serial.println();
    Serial.println(kDevName + " staring...");

    // Sensor init
    Serial.println("[Thermo] Initializing sensors");
    sensors.begin();
    Serial.printf("[Thermo] Found %u sensors\n", sensors.getDeviceCount());

    if (!sensors.getAddress(thermo, 0)) {
        Serial.println("[Thermo] FATAL: Couldn't get sensor address, going to deep sleep.");
        ESP.deepSleep(0);
    }

    // Conectivity init
    Serial.println("[WiFiManager] Initializing...");
    WiFiManager wifiManager;
    bool doAutoConnect = true;

    // Read endpoint config from flash
    String tmpEndpoint = "http://";
    if (SPIFFS.begin() && SPIFFS.exists(kConfigFile)) {
        File conf = SPIFFS.open(kConfigFile, "r");
        tmpEndpoint = conf.readStringUntil('\n');
    } else {
        Serial.println("[Setup] Unable to read config from flash, forcing APMode");
        doAutoConnect = false;
    }

    // Configure WiFiManager
    WiFiManagerParameter endpointParam("endpoint", "Server address", tmpEndpoint.c_str(), 256);
    wifiManager.setSaveConfigCallback([]() -> void {shouldSave = true;});
    wifiManager.addParameter(&endpointParam);

    // Start WiFiManager
    if (doAutoConnect) {
        wifiManager.autoConnect(kAPModeSSID.c_str(), kAPModePassword.c_str());
    } else {
        wifiManager.startConfigPortal(kAPModeSSID.c_str(), kAPModePassword.c_str());
    }

    if (!endpoint.setAddress(endpointParam.getValue())) {
        Serial.println("[Endpoint] Unable to connect, rebooting to APMode in 2s");
        SPIFFS.remove(kConfigFile);
        delay(2000);
        ESP.restart();
    } else {
        Serial.println("[Endpoint] Server check OK");
    }

    if (shouldSave) {
        Serial.println("[Setup] Saving endpoint address to flash");
        File conf = SPIFFS.open(kConfigFile, "w");
        conf.println(endpoint.getAddress());
        conf.close();
    }

    Serial.println("[Setup] Making first contact...");
    bool fcSuccess = false;
    for (int attempt = 0; !fcSuccess && attempt < 5; attempt++) {
        if (attempt > 0) {
            Serial.println("[Setup] Failed. Waiting 5s before retry");
            delay(5000);
        }

        Serial.printf("[Setup] Attempt %i...\n", attempt+1);
        fcSuccess = endpoint.sendFirstContact();
    }

    if (fcSuccess && endpoint.getInterval() != -1) {
        Serial.printf("[Setup] Made first contact. Initial interval is %ims\n", endpoint.getInterval());
    } else {
        Serial.println("[Setup] First contact failed. Going to deep sleep now.");
        ESP.deepSleep(0);
    }


    Serial.println("[Setup] All done, starting main loop");
}

void loop() {
    // Get temperatures
    sensors.requestTemperatures();
    auto temp = sensors.getTempC(thermo);

    Serial.print("[Update] ");
    Serial.print(temp);

    if (!endpoint.sendMeasurementUpdate(temp)) {
        Serial.println(" [FAIL]");
    } else {
        Serial.println(" [ OK ]");
    }

    delay(endpoint.getInterval());
}
