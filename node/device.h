// Device specific constants
// Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
// [original]
// Last change: 2017-11-02

#ifndef device_h
#define device_h

#include <Arduino.h>

// Device name
static const String kDevName = "D1Thermo";

// Pin-out
static const int pinThermo = 5;
static const int pinLed[] = {4, 0, 2};

// Endpoint configuration file path
static const String kConfigFile = "/endpoint.conf";

// AP Mode SSID and password
static const String kAPModeSSID = "ESPConfig";
static const String kAPModePassword = "tazke heslo";

#endif // device_h
