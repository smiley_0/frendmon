// Endpoint class
// Author: Adrian Kiraly <xkiral01@stud.fit.vutbr.cz>
// [original]
// Last change: 2017-11-02

#include "Endpoint.h"

#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>

// Internal URL paths
namespace path {
    static const String api = "/api";
    static const String ping = api + "/ping/";
    static const String firstContact = api + "/first-contact/";
    static const String measurement = api + "/measurement/";
}

int Endpoint::getInterval() {
    return interval;
}

String Endpoint::getAddress() {
    return address;
}

bool Endpoint::setAddress(const String &address) {
    HTTPClient http;

    http.begin(address + path::ping);
    auto code = http.GET();
    http.end();

    // TODO add more sophisticated check
    if (code == HTTP_CODE_OK) {
        this->address = address;
        return true;
    }

    return false;
}

bool Endpoint::sendFirstContact() {
    static const size_t bufferSize = JSON_OBJECT_SIZE(7);
    DynamicJsonBuffer jsonBuffer(bufferSize);

    JsonObject& root = jsonBuffer.createObject();
    root["id"] = ESP.getChipId();
    root["cpu"] = ESP.getCpuFreqMHz();
    root["sdk"] = ESP.getSdkVersion();
    root["core"] = ESP.getCoreVersion();
    root["rst"] = ESP.getResetInfo();
    root["fw"] = ESP.getSketchMD5();
    root["fws"] = ESP.getSketchSize();

    String request;
    root.printTo(request);

    return postMessageAndParse(path::firstContact, request);
}

bool Endpoint::sendMeasurementUpdate(float tempC) {
    static const size_t bufferSize = JSON_OBJECT_SIZE(2);
    DynamicJsonBuffer jsonBuffer(bufferSize);

    JsonObject& root = jsonBuffer.createObject();
    root["id"] = ESP.getChipId();
    root["temp"] = tempC;

    String request;
    root.printTo(request);

    return postMessageAndParse(path::measurement, request);
}

bool Endpoint::parseResponse(const String &response) {
    const size_t bufferSize = JSON_OBJECT_SIZE(3) + 70;
    DynamicJsonBuffer jsonBuffer(bufferSize);

    JsonObject& root = jsonBuffer.parseObject(response);

    JsonVariant status = root["status"];
    if (!status.success() || status.as<int>() != 0) {
        Serial.println("[Endpoint] Received error from server");

        JsonVariant what = root["what"];
        if (what.success()) {
            Serial.println(what.as<char*>());
        }

        return false;
    }

    // Parse interval
    JsonVariant interval = root["int"];
    if (interval.success()) {
        this->interval = interval.as<int>() * 1000;
    }

    return true;
}

bool Endpoint::postMessageAndParse(const String &apiPath, const String &request) {
    HTTPClient http;
    http.begin(address + apiPath);
    http.addHeader("Content-Type", "application/json");
    auto code = http.POST(request);
    auto content = http.getString();
    http.end();

    if (code == HTTP_CODE_OK) {
        return parseResponse(content);
    }

    return false;
}
